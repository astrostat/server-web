class AddColumnsToPurchases < ActiveRecord::Migration[5.0]
  def change
    add_reference :purchases, :in_app_subscription, foreign_key: true
  end
end
