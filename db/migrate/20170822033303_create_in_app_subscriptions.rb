class CreateInAppSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :in_app_subscriptions do |t|
      t.string :name
      t.float :price

      t.timestamps
    end
  end
end
