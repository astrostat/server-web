class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sms_verification, :boolean, default: false
    add_column :users, :admin_verification, :boolean, default: false
    add_column :users, :record_request, :boolean, default: false
    add_column :users, :verification_key, :string
    add_column :users, :check_in_time, :datetime, default: nil
  end
end
