class CreateUserDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :user_details do |t|
      t.belongs_to :user, foreign_key: true

      t.string :nickname
      t.integer :age
      t.integer :height
      t.string :weight
      t.string :job
      t.string :character
      t.string :style
      t.string :smoke
      t.string :drink
      t.string :religion
      t.string :blood
      t.string :gender
      t.string :home
      t.string :interest
      t.string :hobby

      t.attachment :pic1
      t.attachment :pic2
      t.attachment :pic3
      t.attachment :introduce
      t.string :question1
      t.attachment :voice1
      t.string :question2
      t.attachment :voice2
      t.string :question3
      t.attachment :voice3

      t.timestamps
    end
  end
end
