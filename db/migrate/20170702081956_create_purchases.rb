class CreatePurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :purchases do |t|
      t.belongs_to :in_app_product, foreign_key: true
      t.belongs_to :user, foreign_key: true

      t.string :device_os
      t.string :product_id
      t.string :order_id
      t.string :purchase_token
      t.string :purchase_time
      t.string :purchase_state
      t.string :receipt_signature
      t.string :receipt_data
      t.string :developer_payload

      t.timestamps
    end
  end
end
