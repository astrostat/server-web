class CreateBadassReports < ActiveRecord::Migration[5.0]
  def change
    create_table :badass_reports do |t|
      t.integer :reported_user_id
      t.integer :badass_user_id
      t.integer :conversation_id
      t.timestamps
    end
  end
end
