class DeletePinInPhoneNumbers < ActiveRecord::Migration[5.0]
  def change
    remove_column :phone_numbers, :pin
  end
end
