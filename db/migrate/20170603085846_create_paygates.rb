class CreatePaygates < ActiveRecord::Migration[5.0]
  def change
    create_table :paygates do |t|
      t.string :imp_uid
      t.string :pg_provider
      t.boolean :permission
      t.boolean :validation
      t.integer :amount
      t.string :name
      t.string :status
      t.string :receipt_url
      t.string :pay_method
      t.string :merchant_uid
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
