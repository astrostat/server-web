class ChangeColumnToNewRelatedToPurchases < ActiveRecord::Migration[5.0]
  def change
    change_column(:in_app_products, :price, :float, default: 0)
    change_column(:in_app_subscriptions, :price, :float, default: 0)
  end
end
