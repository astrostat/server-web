class CreateLoveletters < ActiveRecord::Migration[5.0]
  def change
    create_table :loveletters do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :matching, foreign_key: true

      t.timestamps
    end
  end
end
