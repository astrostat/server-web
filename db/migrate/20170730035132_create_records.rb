class CreateRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :records do |t|
      t.string :request_days
      t.string :request_times
      t.boolean :voice_status, default: false

      t.belongs_to :user

      t.timestamps
    end
  end
end
