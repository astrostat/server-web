class CreateRentals < ActiveRecord::Migration[5.0]
  def change
    create_table :rentals do |t|
      t.references :hire
      t.references :hired_user
      t.boolean :already_shown, default: false

      t.timestamps
    end
  end
end
