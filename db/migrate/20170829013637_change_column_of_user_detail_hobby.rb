class ChangeColumnOfUserDetailHobby < ActiveRecord::Migration[5.0]
  def change
    remove_column :user_details, :hobby
    add_column :user_details, :attraction, :string
  end
end
