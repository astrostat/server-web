class CreateWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :wallets do |t|
      t.belongs_to :user, foreign_key: true
      t.integer :coin, default: 0

      t.timestamps
    end
  end
end
