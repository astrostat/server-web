class CreateVerifyPhoneNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :verify_phone_numbers do |t|
      t.references :phone_number
      t.string :entered_pin
      t.string :pin
      t.string :verified

      t.timestamps
    end
  end
end
