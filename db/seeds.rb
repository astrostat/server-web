# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Staff.create(email: 'admin@gmail.com', password: 'gksruf', remember_created_at: Time.now)


10.times do
  girl = User.create(email: Faker::Internet.unique.email, password: 'gksruf', remember_created_at: Time.now, sms_verification: true, admin_verification: true)
  girl.create_phone_number(
      {
          :phone_number => Faker::PhoneNumber.unique.cell_phone,
          :pin => Faker::Number.between(0000, 9999),
          :verified => true
      }
  )
  girl.create_user_detail(
      {
          :nickname => Faker::Pokemon.unique.name, :age => Faker::Number.between(20, 39),
          :gender => 'F', :home => 'seoul', :interest => ['스타벅스', '영화보러가기', '쇼핑하기', '발레', '볼빨간사춘기'],
          :attraction => ['요가 7년차 몸매', '항상 잘 웃는 얼굴', '아담한 키', '큰 눈'], :height => Faker::Number.between(150, 169), :job => '발레리나',
          :weight => '보통', :character => '외향적인편', :style => '자유분방한', :smoke => '비흡연', :drink => '맥주 한잔', :religion => '무교', :blood => 'A형',
          :pic1 => File.open('app/assets/images/missing.jpg'),
          :pic2 => File.open('app/assets/images/missing.jpg'),
          :pic3 => File.open('app/assets/images/missing.jpg'),
          :introduce => File.open('app/assets/audios/sample.mp3'),
          :question1 => '연인과 가장 가고싶은 여행지는 ?',
          :voice1 => File.open('app/assets/audios/sample.mp3'),
          :question2 => '인생영화는 무엇인가요 ?',
          :voice2 => File.open('app/assets/audios/sample.mp3'),
          :question3 => '이성을 몇명 사귀어 봤나요 ?',
          :voice3 => File.open('app/assets/audios/sample.mp3'),
      }
  )

  boy = User.create(email: Faker::Internet.unique.email, password: 'gksruf', remember_created_at: Time.now, sms_verification: true, admin_verification: true)
  boy.create_phone_number(
      {
          :phone_number => Faker::PhoneNumber.unique.cell_phone,
          :pin => Faker::Number.between(0000, 9999),
          :verified => true
      }
  )
  boy.create_user_detail(
      {
          :nickname => Faker::Pokemon.unique.name, :age => Faker::Number.between(20, 39),
          :gender => 'M', :home => 'seoul', :interest => ['ruby on rails', 'react-native'],
          :attraction => ['singing', 'programming'], :height => Faker::Number.between(150, 169), :job => '개발자',
          :weight => '보통', :character => '외향적인편', :style => '자유분방한', :smoke => '비흡연', :drink => '맥주 한잔', :religion => '무교', :blood => 'A형',
          :pic1 => File.open('app/assets/images/missing.jpg'),
          :pic2 => File.open('app/assets/images/missing.jpg'),
          :pic3 => File.open('app/assets/images/missing.jpg'),
          :introduce => File.open('app/assets/audios/sample.mp3'),
          :question1 => '첫 경험은 언제인가요 ?',
          :voice1 => File.open('app/assets/audios/sample.mp3'),
          :question2 => '첫 키스 장소는 ?',
          :voice2 => File.open('app/assets/audios/sample.mp3'),
          :question3 => '가고싶은 여행지는 ?',
          :voice3 => File.open('app/assets/audios/sample.mp3'),
      }
  )
end

InAppProduct.create(
    {
        :name => 'com.chalnative.inapp.10_mango', :price => 10
    }
)
InAppProduct.create(
    {
        :name => 'com.chalnative.inapp.30_mango', :price => 30
    }
)
InAppProduct.create(
    {
        :name => 'com.chalnative.inapp.50_mango', :price => 50
    }
)
InAppProduct.create(
    {
        :name => 'com.chalnative.inapp.100_mango', :price => 100
    }
)
InAppProduct.create(
    {
        :name => 'android.test.purchased', :price => 10
    }
)
InAppSubscription.create(
    {
        :name => 'com.chalnative.inapp.60_sub_mango', :price => 60
    }
)
