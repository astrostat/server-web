require 'test_helper'

class ProductsServiceTest < ActiveSupport::TestCase
  test "내가 빌렸던 상품" do
    current_user = users(:girl)
    current_user.save

    boys = %w(boy boy2 boy3 boy4 boy5 boy6)
    boy_users = {}
    boy_user = users(:boy)
    boy_user.save
    boys.each do |boy|
      user = users(boy.to_sym)
      user.save
      boy_users[boy.to_sym] = user
    end

    box = ProductsService.new(current_user)
    box.rentals.count
  end
end
