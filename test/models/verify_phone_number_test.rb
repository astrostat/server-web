require 'test_helper'

class VerifyPhoneNumberTest < ActiveSupport::TestCase
  def setup

  end

  test "pin 인증 생성" do
    current_user = users(:girl)
    phone = current_user.create_phone_number
    pins = []
    3.times do
      verify_phone_number = phone.generate_pin
      pins << verify_phone_number.pin
    end

    assert_equal pins.count, 3
    assert_equal phone.verify_phone_numbers.count, 3
  end

  test 'pin 검증 실패시, 같은 pin 을 가진 새로운 verify phone number 객체 생성' do
    current_user = users(:girl)
    phone = current_user.create_phone_number
    verify_phone_number = phone.generate_pin
    numbers = %w(1111 2222)
    wrong_number = (numbers - [verify_phone_number.pin]).first
    new_verify_number = verify_phone_number.verify(wrong_number)
    assert_equal new_verify_number.pin, verify_phone_number.pin
  end

  test 'pin 검증 3번 실패시, 다른 pin 을 가진 새로운 verify phone number 객체 생성' do
    current_user = users(:girl)
    phone = current_user.create_phone_number
    verify_phone_number = phone.generate_pin
    numbers = %w(1111 2222)
    wrong_number = (numbers - [verify_phone_number.pin]).first
    # 이거 한 번 틀림
    new_verify_number = verify_phone_number.verify(wrong_number)
    # 그리고 열 번 더 틀림
    10.times do
      new_verify_number = new_verify_number.verify(wrong_number)
    end
    not_matched_pins = phone.verify_phone_numbers.where(verified: 'not_matched')
    # 총 시도 11번
    assert_equal not_matched_pins.count, 11
    # 총 시도 11번 하는 동안 pin 이 바뀐 횟수
    assert_equal (not_matched_pins.pluck(:pin) - [verify_phone_number.pin]).uniq.count, 3
  end
end
