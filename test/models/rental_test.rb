require 'test_helper'

class RentalTest < ActiveSupport::TestCase
  test "내가 빌렸던 상품" do
    girl_1 = User.create(email: 'girl@gmail.com', password: 'gksruf', remember_created_at: Time.now)
    me = User.create(email: 'me@gmail.com', password: 'gksruf', remember_created_at: Time.now)
    rental = Rental.new(hire: me, hired_user: girl_1)
    rental.save
    hired_user = me.hired_users.pluck(:email) == ['girl@gmail.com']
    assert hired_user
    my_hire = me.hires.empty?
    assert my_hire
  end
end
