require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @email = 'onesup.lee@gmail.com'
    @auth = {
      provider: 'facebook',
      uid: '111',
      info: {
        email: @email,
        name: '이원섭',
        image: nil
      }
    }
  end

  test "페북으로만 한 번만 로그인" do
    user = User.from_omniauth(Auth.new(@auth))
    assert_equal user.persisted?, true
  end

  test "페북 로그인 후 페북 로그인" do
    # 페이스북으로 처음 로그인 시도하고 (create)
    User.from_omniauth(Auth.new(@auth))
    # 다시 한 번 로그인 시도하는 상황 (find)
    user = User.from_omniauth(Auth.new(@auth))
    assert_equal user.persisted?, true
  end

  test "이메일로 가입 후 페북 로그인" do
    # 페이스북으로 처음 로그인 시도하고 (create)
    User.create(email: @email, password: 'password')
    # 다시 한 번 로그인 시도하는 상황 (find)
    user = User.from_omniauth(Auth.new(@auth))
    assert_equal user.persisted?, true
  end

  class Info
    def initialize(params)
      @params = params
      build_attributes
    end

    private
    def build_attributes
      keys = @params.keys.map{|k| k.downcase}
      keys.each do |key|
        instance_variable_set("@#{key}", eval("@params[:#{key}]"))
        self.class.instance_eval do
          attr_reader key.to_sym
        end
      end
    end
  end

  class Auth
    def initialize(params)
      @params = params
      build_attributes
    end

    def info
      return Info.new(@params[:info])
    end

    private
    def build_attributes
      keys = @params.keys.map{|k| k.downcase}
      keys.each do |key|
        instance_variable_set("@#{key}", eval("@params[:#{key}]"))
        self.class.instance_eval do
          attr_reader key.to_sym unless key == :info
        end
      end
    end
  end
end
