require 'test_helper'

class PhoneNumberTest < ActiveSupport::TestCase
  test "pin 검증" do
    current_user = users(:girl)
    phone = current_user.create_phone_number
    verify_phone_number = phone.generate_pin
    numbers = %w(1111 2222)
    wrong_number = (numbers - [verify_phone_number.pin]).first
    10.times do
      puts phone.verify(wrong_number)
    end
  end
end
