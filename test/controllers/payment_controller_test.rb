require 'test_helper'

class PaymentControllerTest < ActionDispatch::IntegrationTest
  test "should get payform" do
    get payment_payform_url
    assert_response :success
  end

  test "should get gateway" do
    get payment_gateway_url
    assert_response :success
  end

end
