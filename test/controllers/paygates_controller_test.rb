require 'test_helper'

class PaygatesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get paygates_index_url
    assert_response :success
  end

  test "should get new" do
    get paygates_new_url
    assert_response :success
  end

  test "should get show" do
    get paygates_show_url
    assert_response :success
  end

  test "should get create" do
    get paygates_create_url
    assert_response :success
  end

  test "should get mobile" do
    get paygates_mobile_url
    assert_response :success
  end

  test "should get update" do
    get paygates_update_url
    assert_response :success
  end

  test "should get refund" do
    get paygates_refund_url
    assert_response :success
  end

  test "should get verify" do
    get paygates_verify_url
    assert_response :success
  end

  test "should get destroy" do
    get paygates_destroy_url
    assert_response :success
  end

end
