require 'test_helper'

class PhoneNumbersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get phone_numbers_new_url
    assert_response :success
  end

  test "should get create" do
    get phone_numbers_create_url
    assert_response :success
  end

  test "should get verify" do
    get phone_numbers_verify_url
    assert_response :success
  end

end
