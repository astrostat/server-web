$(document).ready(function() {
    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input, number) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img-preview-'+number).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#picture-1").change(function() {
        readURL(this, 1);
    });
    $("#picture-2").change(function() {
        readURL(this, 2);
    });
    $("#picture-3").change(function() {
        readURL(this, 3);
    });
});
