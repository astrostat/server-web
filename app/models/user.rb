class User < ApplicationRecord
  rolify
  acts_as_messageable
  include Pay
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook]

  after_create :set_default_role
  after_create :set_wallet
  before_create :generate_verification_key

  has_one :user_detail, dependent: :destroy
  has_one :phone_number, dependent: :destroy
  has_one :record

  has_many :proposes
  has_many :matchings, through: :proposes
  has_many :loveletters
  has_many :matchings, through: :loveletters

  has_many :hire_rentals, class_name: 'Rental', foreign_key: 'hired_user_id'
  has_many :hires, class_name: 'User', through: :hire_rentals

  has_many :hired_rentals, class_name: 'Rental', foreign_key: 'hire_id'
  has_many :hired_users, class_name:'User', through: :hired_rentals

  has_many :paygates
  has_many :purchases
  has_one :wallet, dependent: :destroy

  belongs_to :report

  def destroy
    update(email: "deleted-#{email}", uid: "deleted-#{uid}")
  end

  def name
    return user_detail.nickname if user_detail.present?
    self[:name]
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.from_omniauth(auth)
    if exists?(provider: auth.provider, uid: auth.uid)
      where(provider: auth.provider, uid: auth.uid).first
    elsif exists?(provider: nil, email: auth.info.email)
      user = where(provider: nil, email: auth.info.email).first
      user.provider = auth.provider
      user.uid = auth.uid
      user.save
      user
    else
      create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]
        user.name = auth.info.name   # assuming the user model has a name
        user.image = auth.info.image # assuming the user model has an image
      end
    end
  end

  private
    def set_default_role
      add_role :user
    end

    def set_wallet
      self.create_wallet(:coin => 20)
    end

    def generate_verification_key
      key = SecureRandom.urlsafe_base64(7).tr('lIO0', 'sxyz') + Time.now.strftime("%Y%m%d%H%M%S")
      self.verification_key = key
    end

end
