class BadassReport < ApplicationRecord
  belongs_to :badass_user, class_name: 'User', foreign_key: 'badass_user_id'
  belongs_to :reported_user, class_name: 'User', foreign_key: 'reported_user_id'
  belongs_to :conversation, :class_name  => "Mailboxer::Conversation"
end
