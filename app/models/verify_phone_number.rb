class VerifyPhoneNumber < ApplicationRecord
  belongs_to :phone_number

  def verify(entered_pin)
    verify_phone_number = self
    if pin == entered_pin
      update(verified: 'verified')
    else
      verify_phone_number = verify_fail
    end
    verify_phone_number
  end

  private

  def verify_fail
    update(verified: 'not_matched')
    if phone_number.not_matched_pins(pin) > 2
      return phone_number.generate_pin
    end
    phone_number.generate_pin(pin)
  end
end
