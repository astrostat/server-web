class Matching < ApplicationRecord
  has_one :propose
  has_one :sender, through: :propose, source: :user, dependent: :destroy
  has_one :loveletter
  has_one :receiver, through: :loveletter, source: :user, dependent: :destroy
end
