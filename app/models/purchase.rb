class Purchase < ApplicationRecord
  belongs_to :in_app_product
  belongs_to :in_app_subscription
  belongs_to :user
end
