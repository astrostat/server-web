class PhoneNumber < ApplicationRecord
  attr_accessor :pin
  
  belongs_to :user
  has_many :verify_phone_numbers

  def twilio_client
    Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
  end

  def generate_pin(generate_pin = random_pin)
    verify_phone_numbers.create(pin: generate_pin, verified: 'not_yet')
  end

  def send_pin
    twilio_client.messages.create(
        {
            :to => phone_number,
            :from => ENV['TWILIO_PHONE_NUMBER'],
            :body => "번호 : #{valid_verify_phone_number.pin}"
        }
    )
  end

  def verify(entered_pin)
    result = ''
    verify_phone_number = valid_verify_phone_number.verify(entered_pin)
    verify_phone_number.verified
    if verify_phone_number.verified == 'verified'
      user.update(sms_verification: true)
      update(verified: true)
      result = verify_phone_number.verified
    else
      update(verified: true)
      user.update(sms_verification: false)
      result = 'not_matched'
      result = 'pin_changed' if not_matched_pins(verify_phone_number.pin) < 1
    end
    result
  end

  def not_matched_pins(pin)
    verify_phone_numbers.where(verified: 'not_matched', pin: pin).count
  end

  private

  def valid_verify_phone_number
    verify_phone_numbers.where(verified: 'not_yet').last
  end

  def random_pin
    rand(0000..9999).to_s.rjust(4, "0")
  end
end
