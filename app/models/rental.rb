class Rental < ApplicationRecord
  belongs_to :hire, class_name: 'User', foreign_key: 'hire_id'
  belongs_to :hired_user, class_name: 'User', foreign_key: 'hired_user_id'
end
