class JsonData
  include ActiveModel::Serialization

  def initialize(data)
    @data = data
  end
end
