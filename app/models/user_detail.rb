class UserDetail < ApplicationRecord
  belongs_to :user

  has_attached_file :introduce, :preserve_files => true
  validates_attachment_content_type :introduce, :content_type => %w( audio/basic audio/mpeg audio/wav audio/x-wav )
  has_attached_file :voice1, :preserve_files => true
  validates_attachment_content_type :voice1, :content_type => %w( audio/basic audio/mpeg audio/wav audio/x-wav )
  has_attached_file :voice2, :preserve_files => true
  validates_attachment_content_type :voice2, :content_type => %w( audio/basic audio/mpeg audio/wav audio/x-wav )
  has_attached_file :voice3, :preserve_files => true
  validates_attachment_content_type :voice3, :content_type => %w( audio/basic audio/mpeg audio/wav audio/x-wav )

  has_attached_file :pic1, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic1, content_type: /\Aimage\/.*\Z/
  has_attached_file :pic2, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic2, content_type: /\Aimage\/.*\Z/
  has_attached_file :pic3, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic3, content_type: /\Aimage\/.*\Z/
  has_attached_file :pic4, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic4, content_type: /\Aimage\/.*\Z/
  has_attached_file :pic5, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic5, content_type: /\Aimage\/.*\Z/
  has_attached_file :pic6, styles: {xhdpi_4by3: "720x540>", medium: "300x300>", thumb: "160x160>"}, default_url: "/images/upload.png", path: ":rails_root/public/system/:class/:user_phone/:attachment/:style/:filename", :preserve_files => true
  validates_attachment_content_type :pic6, content_type: /\Aimage\/.*\Z/

  #validates :nickname, :age, :height, :weight, :job, :character, presence: true
  #validates :style, :smoke, :drink, :religion, :blood, :gender, :home, presence: true
  #validates :interest, :attraction, presence: true
  #validates :pic1, :pic2, :pic3, presence: true
  #validates :question1, :question2, :question3, presence: true

end
