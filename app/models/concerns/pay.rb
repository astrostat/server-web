module Pay
  def purchase(price)
    coin = self.wallet.coin
    if coin_check(coin, price)
      self.wallet.coin -= price
      self.wallet.save
      true
    else
      false
    end
  end

  private

  def coin_check(coin, price)
    if coin >= price
      coin
    else
      nil
    end
  end

end