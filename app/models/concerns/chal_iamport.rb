class ChalIamport
  def initialize(user, pay_params)
    @user = user
    @pay_params = pay_params
  end

  def paycess
    parsed_imp = Iamport.payment(@pay_params[:imp_uid]).parsed_response['response']
    imp = parsed_imp.reject { |key, value| !Paygate.attribute_names.include?(key) }

    if parsed_imp['status'] == 'paid' or parsed_imp.nil? # payment succeed -> build object or faulty request
      @paygate = @user.paygates.new(imp)
      @paygate.permission = true
      @paygate.validation = true if parsed_imp['status'] == 'paid' # Iamport API call verification
    end
    @paygate
  end

end
