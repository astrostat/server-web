module ApplicationHelper

  def flash_toastr
    flash_messages = []
    flash.each do |type, message|
      if type == 'notice'
        type = 'info'
      elsif type == 'success'
        type = 'success'
      elsif type == 'error' or type == 'alert'
        type = 'error'
      else
        type = 'info'
      end
      #type = 'success' if type == 'notice'
      #type = 'error'   if type == 'alert'
      text = "<script>toastr.#{type}('#{message}','',{ 'positionClass': 'toast-bottom-right', 'timeOut': '2000' });</script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe
  end

end
