module HomeHelper
  def where_we_are
    case controller.controller_name
      when 'user_details'
        case controller.action_name
          when 'new'
            return '프로필 입력'
          when 'create'
            return '프로필 입력'
          when 'edit'
            return '회원정보수정'
          else
            '회원가입'
        end
      when 'sessions'
        case controller.action_name
          when 'new'
            return '로그인'
          else
            '로그인'
        end

      when 'phone_numbers'
        case controller.action_name
          when 'new'
            return '번호인증'
          else
            '번호인증'
        end

      when 'records'
        case controller.action_name
          when 'show'
            '인터뷰'
          when 'new'
            '인터뷰 신청'
          else
            '인터뷰'
        end

      when 'registrations'
        case controller.action_name
          when 'new'
            return '회원가입'
          else

        end

      when 'passwords'
        case controller.action_name
          when 'new'
            return '비밀번호 찾기'
          else
        end
      else
        '하와이안 커플'
    end
  end

  def progress_bar
    case controller.controller_name
      when 'user_details'
        case controller.action_name
          when 'new'
            return 25
          when 'create'
            return '프로필 입력'
          else
            '회원가입'
        end
      when 'sessions'
        case controller.action_name
          when 'new'
            return '로그인'
          else
            '로그인'
        end

      when 'phone_numbers'
        case controller.action_name
          when 'new'
            return 1
          else
            '번호인증'
        end
      when 'home'

      when 'registrations'
        case controller.action_name
          when 'new'
            return '회원가입'
          else

        end
      else
        '하와이안 커플'
    end
  end

  def back_button
    case controller.controller_name
      when 'records'
        case controller.action_name
          when 'create'
            return false
          else
            true
        end
      when 'user_details'
        return false
      when 'phone_numbers'
        return false

      else
        true
    end
  end
end
