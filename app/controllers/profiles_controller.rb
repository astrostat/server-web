class ProfilesController < AppController
  def show
    @user = current_user
  end

  def friend
    @user = User.find(params[:id])
  end
end
