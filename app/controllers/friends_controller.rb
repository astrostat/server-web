class FriendsController < ApplicationController
  def index
    box = ProductsService.new(current_user)
    @friends = box.purchased
  end

  def show
    box = ProductsService.new(current_user)
    msg = MessageService.new(current_user)
    @friends = box.purchased + msg.receivings
    @friend = User.find(params[:id])

    redirect_to '/' unless @friends.include?(@friend)
  end

end
