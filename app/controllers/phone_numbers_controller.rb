class PhoneNumbersController < AppController

  skip_before_action :verify_sms_verification

  def new
    if current_user.phone_number
      @phone_number = PhoneNumber.new
    else
      @phone_number = current_user.build_phone_number
    end
  end

  def create
    phone_number = format_number(params[:phone_number][:phone_number])
    @phone_number = PhoneNumber.find_or_create_by(phone_number: phone_number, user: current_user)
    @phone_number.generate_pin
    if @phone_number.send_pin
      redirect_to verify_phone_numbers_path
    else
      redirect_to new_phone_number_path, status: 400, notice: '개발자에게 문의해주세요. ※인증번호발송관련※'
    end
  end

  def verify
    @phone_number = PhoneNumber.find_by(phone_number: params[:hidden_phone_number], user: current_user)
    @phone_number.verify(params[:pin])
    respond_to do |format|
      format.js
    end
  end

  private

    def format_number(phone_number)
      "+82" +  phone_number.split('-').join
    end
end
