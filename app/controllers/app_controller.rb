class AppController < ApplicationController
  layout "app"

  skip_before_action :authenticate_user!

  def index
    redirect_to root_path if user_signed_in?
  end

end
