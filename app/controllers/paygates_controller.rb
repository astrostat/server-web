class PaygatesController < ApplicationController
  before_action :request_params, only: [:new]
  before_action :paygate_params, only: [:create]
  before_action :mobile_paygate_params, only: [:mobile]
  before_action :set_paygate, only: [:show, :update, :refund, :destroy]

  # GET /paygates
  def index

  end

  def new
    @req = request_params
  end

  # GET /paygates/:id
  def show

  end

  # POST /paygates
  def create
    chal = ChalIamport.new(current_user, paygate_params)
    @paygate = chal.paycess

    if @paygate.save
      wallet = current_user.wallet
      wallet.coin += @paygate.amount/10
      wallet.save
      redirect_to profile_path
      #render json: @paygate, status: :created, location: @paygate # 피씨 유저들을 결제후 보여주는 곳으로 보내야한다.# 모바일유저들을 결제후 보여주는 곳으로 보내야한다.
    else
      redirect_to profile_path
      #render json: @paygate.errors, status: :unprocessable_entity
    end
  end

  # GET /user/:user_key/mobilepay?imp_uid=imp593457298&merchant_uid=2343123&imp_success=success
  def mobile
    user = User.find_by_verification_key(params[:user_key])
    chal = ChalIamport.new(user, mobile_paygate_params)
    @paygate = chal.paycess

    if @paygate.save
      user.coin.wallet += @paygate.amount
      user.save
      redirect_to profile_path
      #render json: @paygate, status: :created, location: @paygate # 모바일유저들을 결제후 보여주는 곳으로 보내야한다.
    else
      redirect_to profile_path
      #render json: @paygate.errors, status: :unprocessable_entity
    end
  end

  # POST /paygates/:id/refund
  # rails generate administrate:views:index Paygate # to modify and add custom action to paygate admin view
  def refund
    render nothing: true, status: :unprocessable_entity and return if @paygate.status == 'refunded' # if already refund has been made, return
    body = {
        imp_uid: @paygate.imp_uid,
        merchant_uid: "chalpayment",
        amount: @paygate.amount
    }
    if Iamport.cancel(body)
      @paygate.validation ^= true
      @paygate.status = 'refunded'
      render json: @paygate if @paygate.save
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  # DELETE /paygates/:id
  def destroy
    @paygate.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_paygate
    @paygate = Paygate.find_by_imp_uid(params[:id]) # require imp_uid from node server
  end

  # Only allow a trusted parameter "white list" through.
  # Parameters: {"paygate"=>{"user_key"=>"33732840", "imp_uid"=>"imp_543774926149", "pg_provider"=>"html5_inicis", "permission"=>"true", "name"=>"쿠니목소리4개", "amount"=>"1000", "status"=>"paid", "receipt_url"=>"https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid=StdpayHPP_INIpayTest20170523170600513186&noMethod=1"}}
  def paygate_params
    params.require(:paygate).permit(:imp_uid, :merchant_uid, :imp_success)
  end

  # Parameters: {"imp_uid"=>"imp_725476179142", "merchant_uid"=>"merchant_1495532476174", "imp_success"=>"true"}
  def mobile_paygate_params
    params.permit(:user_key, :imp_uid, :merchant_uid, :imp_success)
  end

  def request_params
    params.require(:paygate).permit(:user_key, :pg, :pay_method, :merchant_uid, :product_name, :amount, :buyer_email, :buyer_tel, :buyer_name, :buyer_addr, :buyer_postcode)
  end

end
