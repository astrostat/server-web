class Api::ChatController < Api::ApiController
  before_action :find_user

  def call
    redirect_to conversations_path
  end

  def friend
    redirect_to :back
  end

  private

  def find_user
    @user = current_user
  end

end
