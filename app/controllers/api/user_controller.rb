class Api::UserController < Api::ApiController
  before_action :find_user

  def information
    render json: @user
  end

  def profile_image
    if @user.user_detail
      profile_image_url = @user.user_detail.pic1.url
      data = JsonData.new(profile_image_url)
      render json: data
    else
      render nothing: true, status: 401
    end

  end

  private

  def find_user
    @user = current_user
  end

end
