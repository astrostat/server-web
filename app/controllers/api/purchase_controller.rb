class Api::PurchaseController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:verify_product, :verify_subscription]
  before_action :verify_params, only: [:verify_product, :verify_subscription]

  def call
    redirect_to :back
  end

  def expense
    target_url = params[:target_url]
    redirect_to target_url
  end

  def verify_product
    data = params[:data]

    if current_user
      if data.has_key? :device_os and data[:device_os] == 'android'
        wallet = current_user.wallet
        original_coin = wallet.coin
        @purchase = Purchase.new
        @purchase.user = current_user
        @purchase.device_os = data[:device_os]
        @purchase.product_id = data[:productId]
        @purchase.order_id = data[:orderId]
        @purchase.purchase_token = data[:purchaseToken]
        @purchase.purchase_time = data[:purchaseTime]
        @purchase.purchase_state = data[:purchaseState]
        @purchase.receipt_signature = data[:receiptSignature]
        @purchase.developer_payload = data[:developerPayload]
        @purchase.in_app_product = InAppProduct.find_by_name(data[:productId])
        google = GooglePlay.new(@purchase)
        if google.verify_product
          if @purchase.save
            coin_to_be_added = @purchase.in_app_product.price

            wallet.update(coin: original_coin + coin_to_be_added)
            render json: wallet, status: 200
          else
            render nothing: true, status: 500
          end
        else
          render nothing: true, status: 404
        end
      else
        wallet = current_user.wallet
        original_coin = wallet.coin

        receipt = params[:receipt]
        logger.info "verifying receipt: #{receipt}"
        url = URI.parse('https://buy.itunes.apple.com/')
        req = Net::HTTP::Post.new('/verifyReceipt', initheader = { 'Content-Type' => 'application/json'})
        payload = {
            'receipt-data': receipt
        }
        req.body = payload.to_json
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        res = http.request(req)
        result = JSON.parse(res.body)
        logger.info "verifying receipt result: #{result}"

        if res.code.to_i == 200 and result['status'] == 0
          @purchase = Purchase.new
          #@purchase.receipt_b64 = receipt
          @purchase.user = current_user
          @purchase.in_app_product = InAppProduct.find_by_name(result['receipt']['in_app'][0]['product_id'])
          @purchase.attributes = result['receipt'].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          @purchase.attributes = result['receipt']['in_app'][0].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          #@purchase.download_id = 0
          if @purchase.save
            case @purchase.in_app_product.name
              when "#{univ_text}_item_30"
                coin_to_be_added = 30
              when "#{univ_text}_item_90"
                coin_to_be_added = 90
              when "#{univ_text}_item_130"
                coin_to_be_added = 130
              when "#{univ_text}_item_210"
                coin_to_be_added = 210
              else
                coin_to_be_added = 0
            end

            wallet.update(coin: original_coin + coin_to_be_added)
            render json: current_user, status: 200
          else
            render nothing: true, status: 500
          end
        elsif res.code.to_i == 200 and result['status'] > 0
          render json: {status: result['status']}, status: 403
        else
          render nothing: true, status: 403
        end
      end
    else
      render nothing: true, status: 401
    end
  end

  def verify_subscription
    data = params[:data]
    if current_user
      if data.has_key? :device_os and data[:device_os] == 'android'
        wallet = current_user.wallet
        original_coin = wallet.coin
        @purchase = Purchase.new
        @purchase.user = current_user
        @purchase.device_os = data[:device_os]
        @purchase.product_id = data[:productId]
        @purchase.order_id = data[:orderId]
        @purchase.purchase_token = data[:purchaseToken]
        @purchase.purchase_time = data[:purchaseTime]
        @purchase.purchase_state = data[:purchaseState]
        @purchase.receipt_signature = data[:receiptSignature]
        @purchase.developer_payload = data[:developerPayload]
        @purchase.in_app_subscription = InAppSubscription.find_by_name(data[:productId])
        google = GooglePlay.new(@purchase)
        if google.verify_subscription
          if @purchase.save
            # subscription을 서버에서 어떻게 핸들링할지에 대한 코딩이 선행 되어야한다.
            case @purchase.in_app_product.name
              when "10_mango"
                coin_to_be_added = 10
              when "30_mango"
                coin_to_be_added = 30
              when "50_mango"
                coin_to_be_added = 50
              when "100_mango"
                coin_to_be_added = 100
              else
                coin_to_be_added = 0
            end

            wallet.update(coin: original_coin + coin_to_be_added)
            render json: wallet, status: 200
          else
            render nothing: true, status: 500
          end
        else
          render nothing: true, status: 500
        end
      else
        wallet = current_user.wallet
        original_coin = wallet.coin

        receipt = params[:receipt]
        logger.info "verifying receipt: #{receipt}"
        url = URI.parse('https://buy.itunes.apple.com/')
        req = Net::HTTP::Post.new('/verifyReceipt', initheader = { 'Content-Type' => 'application/json'})
        payload = {
            'receipt-data': receipt
        }
        req.body = payload.to_json
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        res = http.request(req)
        result = JSON.parse(res.body)
        logger.info "verifying receipt result: #{result}"

        if res.code.to_i == 200 and result['status'] == 0
          @purchase = Purchase.new
          #@purchase.receipt_b64 = receipt
          @purchase.user = current_user
          @purchase.in_app_product = InAppProduct.find_by_name(result['receipt']['in_app'][0]['product_id'])
          @purchase.attributes = result['receipt'].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          @purchase.attributes = result['receipt']['in_app'][0].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          #@purchase.download_id = 0
          if @purchase.save
            case @purchase.in_app_product.name
              when "#{univ_text}_item_30"
                coin_to_be_added = 30
              when "#{univ_text}_item_90"
                coin_to_be_added = 90
              when "#{univ_text}_item_130"
                coin_to_be_added = 130
              when "#{univ_text}_item_210"
                coin_to_be_added = 210
              else
                coin_to_be_added = 0
            end

            wallet.update(coin: original_coin + coin_to_be_added)
            render json: current_user, status: 200
          else
            render nothing: true, status: 500
          end
        elsif res.code.to_i == 200 and result['status'] > 0
          render json: {status: result['status']}, status: 403
        else
          render nothing: true, status: 403
        end
      end
    else
      render nothing: true, status: 401
    end
  end

  private

    def verify_params
      params.require(:data).permit(
          :device_os, :productId, :orderId,
          :purchaseToken, :purchaseTime, :purchaseState,
          :receiptSignature, :receiptData, :developerPayload
      )

    end
end
