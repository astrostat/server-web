class Api::BankController < Api::ApiController
  before_action :find_user
  #before_action :find_user_by_cookie

  def wallet
    wallet = @user.wallet
    render json: wallet
  end

  private

    def find_user
      @user = current_user
    end

end
