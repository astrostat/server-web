class MatchingsController < ApplicationController

  def index
    box = ProductsService.new(current_user)
    rentals = box.rentals.reverse # 하루에 2개씩만 볼 수 있는 로직 필요

    @hired_users = []
    rentals.each do |rental|
      @hired_users << rental.hired_user
    end
  end

  # Post 추가렌탈
  def create
    em = 5
    box = ProductsService.new(current_user)
    if current_user.purchase(em)
      box.extra_rentals
    else
      redirect_to api_purchase_call_path and return
    end
    redirect_to api_purchase_expense_path(target_url: matchings_path)
  end

  def show
    mp = 5
    if current_user.purchase(mp)
      @friend = User.find(params[:id])
      Matching.create(
          {
              sender: current_user, receiver: @friend
          }
      )
      current_user.hired_rentals.find_by_hired_user_id(@friend.id).destroy
      redirect_to api_purchase_expense_path(target_url: friend_path(@friend))
    else
      redirect_to api_purchase_call_path
    end
  end
end
