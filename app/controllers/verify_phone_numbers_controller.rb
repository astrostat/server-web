class VerifyPhoneNumbersController < AppController

  skip_before_action :verify_sms_verification

  def index
    @phone_number = current_user.phone_number
  end

  def create
    @phone_number = current_user.phone_number
    verify = @phone_number.verify(phone_number_params[:pin])
    if verify == 'verified'
      redirect_to new_user_detail_path
    else
      @phone_number.send_pin if verify == 'pin_changed'
      redirect_to verify_phone_numbers_path, notice: I18n.t("notice.verify_phone.#{verify}")
    end
  end

  private

  def phone_number_params
    params.require(:phone_number).permit(:pin)
  end

end
