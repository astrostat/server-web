class PaymentController < ApplicationController
    # before_action :payform_params, only: [:payform]
    before_action :request_params, only: [:gateway]
    # before_action :user_params, only: [:gateway]
    layout 'api', only: [:gateway]
    # Post
    def payform
      @user = current_user
      render :payform
    end

    # Post
    def gateway
      # need to render some js files in order to operate payment gateway layer
      @req = request_params # JSON from Client ( required informations for pg process )
      render :gateway
    end

    private

    def payform_params
      params.require(:paygate).permit(:id, :user_key, :buyer_name)
    end

    def request_params
      params.require(:paygate).permit(:user_key, :pg, :pay_method, :merchant_uid, :product_name, :amount, :buyer_email, :buyer_tel, :buyer_name)
    end

    def user_params
      params.require(:paygate).permit(:user_key, :buyer_email, :buyer_tel, :buyer_name)
    end

end
