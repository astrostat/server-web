class RecordsController < ApplicationController
  before_action :get_user_detail, only: [:new, :create]

  def show
  end

  def new
    @days = ['월', '화', '수', '목', '금', '토', '일']
    @times = ['오전', '오후', '저녁']
    @questions = [
        '좋아하는 이성의 타입은 어떻신가요?', '사귀어본 이성의 수는 몇명인가요?', '이성에게 한눈에 반했던 적이 있나요?', '특이한 곳에서 데이트 해본 적이 있나요?', '사귀는 사람과 얼마나 자주 연락하나요?', '커플 아이템으로 해보고 싶은게 있다면 어떤게 있나요?',
        '이성의 옷 취향이 어떻신가요?', '본인이 주로 입는 옷 취향은 어떻신가요?', '해 본 선물 중에 상대방이 가장 좋아했던 것은 무엇인가요?', '연인과 함께 여행을 간다면 어디에 가고싶으신가요?',
        '전 이성친구 왜 헤어졌나요?', '데이트할땐 주로 무얼 하시나요?',
        '할 줄 아는 요리는 어떤게 있나요?', '친구들과 어떻게 시간을 보내시나요?', '주말은 주로 어떻게 보내시나요?',
        '좋아하는 책 3가지는 어떤게 있나요?', '좋아하는 음식이나 음료에는 어떤게 있나요?', '가장 기억에 남는 여행지는 어디이고 어땠나요?', '평소에 자주 가는 장소가 있다면 어디이고 왜 가시나요?',
        '면허 혹은 차가 있으신가요?', '아침형 VS 저녁형 인간 중 어디에 가까우신가요?',
        '자신에게 주는 상으로 어떤 것 까지 줘보신 경험이 있나요?', '현재 자취하고 계신가요? 하신다면 어디쪽에 계신가요?',
        '집안일에 능숙한 편인가요?', '인터넷으로는 주로 뭘 하시나요?'
    ]
  end

  def create
    if params[:record].nil?
      redirect_to new_record_path, notice: '인터뷰 가능한 요일과 시간을 알려주시겠어요?' and return
    end
    current_user.record.destroy if current_user.record
    record = current_user.build_record(record_params)

    if record.save
      @user_detail.update(user_detail_params)

      # 녹음신청이 처음인 유저만 추가 포인트를 충전해준다.

      if current_user.record_request
        flash[:notice] = '녹음시간이 다시 재신청 되었습니다.'
      else
        wallet = current_user.wallet
        original_coin = wallet.coin
        coin_to_be_added = 20
        wallet.update(coin: original_coin + coin_to_be_added)
      end

      current_user.record_request = true
      current_user.save
    end

  end

  def edit
  end

  def update
  end

  def delete
  end

  private

  def get_user_detail
    @user_detail = current_user.user_detail
  end

  def record_params
    params.require(:record).permit(
        {:request_days => []},
        {:request_times => []}
    )
  end

  def user_detail_params
    params.require(:user_detail).permit(
        :question1,
        :question2,
        :question3
    )
  end

end
