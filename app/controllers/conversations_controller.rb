class ConversationsController < ApplicationController
  before_action :authenticate_user!
  helper_method :mailbox, :conversation
  layout 'app', only: [:show]

  def index
  end

  def new
    @recipients = User.find(params[:recipients])
    msg = MessageService.new(current_user)
    if msg.sendings.include?(@recipients)
      chat = msg.find_chat(@recipients)
      redirect_to conversation_path(chat)
    end
  end

  def show
    conversation.receipts_for(current_user).each do |unread_receipt|
      unless unread_receipt.is_read
        unread_receipt.update(is_read: true) # 메세지 읽음으로 표시
      end
    end
    ActionCable.server.broadcast("read_conversations_#{conversation.id}", user_id: current_user.id)
  end

  def inbox
  end

  def sentbox
  end

  def create
    msg = MessageService.new(current_user)
    recipients = User.find(conversation_params(:recipients))

    unless current_user.admin_verification
      redirect_to :back, notice: '관리자의 프로필 승인대기가 이루어져야 메세지를 보내실 수 있습니다.' and return
    end

    if msg.sendings.include?(recipients)
      redirect_to conversations_path, notice: '이미 대화중인 상대방 입니다' and return
    end

    cp = 5
    if current_user.purchase(cp)
        conversation = current_user.send_message(recipients, *conversation_params(:body, :subject)).conversation
        redirect_to api_purchase_expense_path(target_url: api_chat_call_path(conversation_id: conversation.id))
    else
      redirect_to api_purchase_call_path
    end
  end

  def reply
    receiver = (conversation.recipients - [current_user]).first
    if conversation.is_completely_trashed?(receiver)
      # redirect_to conversation_path(conversation), notice: '차단되셨습니다' and return
    else
      contents = {}
      # receipt_code 는 해당 메세지 읽었는지 확인하는 용도. receipt_id 와는 다르다.
      contents[:receipt_code] = SecureRandom.urlsafe_base64(4)
      contents[:body] = params[:message][:body]
      contents[:user_id] = current_user.id
      contents[:user_nickname] = current_user.user_detail.nickname
      contents[:user_profile] = current_user.user_detail.pic1.url(:thumb)
      ActionCable.server.broadcast("conversations_#{params[:id]}", contents)
      receipt = current_user.reply_to_conversation(conversation, *message_params(:body, :subject))
    end
  end

  def trash
    conversation.move_to_trash(current_user)
    redirect_to conversation_path(conversation), notice: '차단했습니다.'
  end

  def untrash
    conversation.untrash(current_user)
    redirect_to conversation_path(conversation), notice: '차단 해제했습니다.'
  end

  private

    def mailbox
      @mailbox ||= current_user.mailbox
    end

    def conversation
      @conversation ||= mailbox.conversations.find(params[:id])
    end

    def conversation_params(*keys)
      fetch_params(:conversation, *keys)
    end

    def message_params(*keys)
      fetch_params(:message, *keys)
    end

    def fetch_params(key, *subkeys)
      params[key].instance_eval do
        case subkeys.size
          when 0 then
            self
          when 1 then
            self[subkeys.first]
          else
            subkeys.map { |k| self[k] }
        end
      end
    end
end
