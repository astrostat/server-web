class UserDetailsController < HomeController

  before_action :authenticate_user!
  before_action :get_user_detail, only: [:edit, :update]

  skip_before_action :verify_user_detail

  def new
    @locations = ['서울', '경기', '인천', '대전', '충북', '충남', '강원', '부산', '경북', '경남', '대구', '울산', '광주', '전북', '전남', '제주도']
    @questions = [
        '좋아하는 이성의 타입은 어떻신가요?', '사귀어본 이성의 수는 몇명인가요?', '이성에게 한눈에 반했던 적이 있나요?', '특이한 곳에서 데이트 해본 적이 있나요?', '사귀는 사람과 얼마나 자주 연락하나요?', '커플 아이템으로 해보고 싶은게 있다면 어떤게 있나요?',
        '이성의 옷 취향이 어떻신가요?', '본인이 주로 입는 옷 취향은 어떻신가요?', '해 본 선물 중에 상대방이 가장 좋아했던 것은 무엇인가요?', '연인과 함께 여행을 간다면 어디에 가고싶으신가요?',
        '전 이성친구 왜 헤어졌나요?', '데이트할땐 주로 무얼 하시나요?',
        '할 줄 아는 요리는 어떤게 있나요?', '친구들과 어떻게 시간을 보내시나요?', '주말은 주로 어떻게 보내시나요?',
        '좋아하는 책 3가지는 어떤게 있나요?', '좋아하는 음식이나 음료에는 어떤게 있나요?', '가장 기억에 남는 여행지는 어디이고 어땠나요?', '평소에 자주 가는 장소가 있다면 어디이고 왜 가시나요?',
        '면허 혹은 차가 있으신가요?', '아침형 VS 저녁형 인간 중 어디에 가까우신가요?',
        '자신에게 주는 상으로 어떤 것 까지 줘보신 경험이 있나요?', '현재 자취하고 계신가요? 하신다면 어디쪽에 계신가요?',
        '집안일에 능숙한 편인가요?', '인터넷으로는 주로 뭘 하시나요?'
    ]
    @interests = ['독서', '게임', '술', '운동', '영화', '노래', '글쓰기', '페북', '인스타', '여행', '연애', '맛집', '사진', '미술', '요리', '야구', '힙합', '스타벅스', '인디', '창업', '외국어', '봉사', '아이돌']
    @attractions = ['근육질', '글래머']
    @weights = ['마름', '보통', '통통함']
    @characters = ['내성적인편', '외향적인편']
    @styles = ['귀여운', '유머있는', '열정적인', '긍정적인', '끈기있는']
    @smokes = ['흡연', '비흡연']
    @drinks = ['한잔', '반병', '1병', '2병', '3병', '3병 +']
    @religions = ['무교', '기독교', '천주교', '불교', '원불교', '기타']
    @bloods = ['A형', 'B형', 'AB형', 'O형']
    @user_detail = current_user.build_user_detail
  end

  def create
    if current_user.user_detail.update(user_detail_params)
      redirect_to root_path, notice: '가입 성공'
    else
      redirect_back(fallback_location: (request.referer || root_path), notice: "에러발생")
    end
  end

  def image_upload
    if current_user.user_detail
      user_detail = current_user.user_detail
    else
      user_detail = current_user.create_user_detail
    end
    user_detail.pic1 = user_image_params[:pic1]
    user_detail.pic2 = user_image_params[:pic2]
    user_detail.pic3 = user_image_params[:pic3]
    user_detail.pic4 = user_image_params[:pic4]
    user_detail.pic5 = user_image_params[:pic5]
    user_detail.pic6 = user_image_params[:pic6]
    if user_detail.save!(validate: false)
      head :ok
    else
      head :unauthorized
    end
  end

  def edit
    @locations = ['서울', '경기', '인천', '대전', '충북', '충남', '강원', '부산', '경북', '경남', '대구', '울산', '광주', '전북', '전남', '제주도']
    @questions = [
        '좋아하는 이성의 타입은 어떻신가요?', '사귀어본 이성의 수는 몇명인가요?', '이성에게 한눈에 반했던 적이 있나요?', '특이한 곳에서 데이트 해본 적이 있나요?', '사귀는 사람과 얼마나 자주 연락하나요?', '커플 아이템으로 해보고 싶은게 있다면 어떤게 있나요?',
        '이성의 옷 취향이 어떻신가요?', '본인이 주로 입는 옷 취향은 어떻신가요?', '해 본 선물 중에 상대방이 가장 좋아했던 것은 무엇인가요?', '연인과 함께 여행을 간다면 어디에 가고싶으신가요?',
        '전 이성친구 왜 헤어졌나요?', '데이트할땐 주로 무얼 하시나요?',
        '할 줄 아는 요리는 어떤게 있나요?', '친구들과 어떻게 시간을 보내시나요?', '주말은 주로 어떻게 보내시나요?',
        '좋아하는 책 3가지는 어떤게 있나요?', '좋아하는 음식이나 음료에는 어떤게 있나요?', '가장 기억에 남는 여행지는 어디이고 어땠나요?', '평소에 자주 가는 장소가 있다면 어디이고 왜 가시나요?',
        '면허 혹은 차가 있으신가요?', '아침형 VS 저녁형 인간 중 어디에 가까우신가요?',
        '자신에게 주는 상으로 어떤 것 까지 줘보신 경험이 있나요?', '현재 자취하고 계신가요? 하신다면 어디쪽에 계신가요?',
        '집안일에 능숙한 편인가요?', '인터넷으로는 주로 뭘 하시나요?'
    ]
    @interests = ['독서', '게임', '술', '운동', '영화', '노래', '글쓰기', '페북', '인스타', '여행', '연애', '맛집', '사진', '미술', '요리', '야구', '힙합', '스타벅스', '인디', '창업', '외국어', '봉사', '아이돌']
    @attractions = ['근육질', '글래머']
    @weights = ['마름', '보통', '통통함']
    @characters = ['내성적인편', '외향적인편']
    @styles = ['귀여운', '유머있는', '열정적인', '긍정적인', '끈기있는']
    @smokes = ['흡연', '비흡연']
    @drinks = ['한잔', '반병', '1병', '2병', '3병', '3병 +']
    @religions = ['무교', '기독교', '천주교', '불교', '원불교', '기타']
    @bloods = ['A형', 'B형', 'AB형', 'O형']
  end

  def update
    if @user_detail.update(user_detail_params)
      redirect_to profile_path, notice: '회원정보 수정 완료'
    else
      redirect_to :back
    end
  end

  private

  def get_user_detail
    @user_detail = current_user.user_detail
  end

  def user_image_params
    params.require(:user_detail).permit(
        :pic1, :pic2, :pic3, :pic4, :pic5, :pic6
    )
  end

  def user_detail_params
    return params unless params[:user_detail]

    params.require(:user_detail).permit(
        :nickname, :age, :height, :weight, :job, :character, :style, :smoke, :drink,
        :religion, :blood,
        :gender, :app, {:interest => []}, {:attraction => []},
        :question1, :voice1,
        :question2, :voice2,
        :question3, :voice3
    )
  end

end
