class PurchaseController < ApplicationController
  def verify
    if current_user
      if params.has_key? :device_os and params[:device_os] == 'android'
        user_detail = current_user.user_detail
        original_coin = user_detail.wallet.coin
        @purchase = Purchase.new
        @purchase.user = current_user
        @purchase.receipt_b64 = params[:purchaseToken]
        @purchase.transaction_id = params[:orderId]
        @purchase.original_transaction_id = params[:orderId]
        @purchase.in_app_product = InAppProduct.find_by_name(params[:productId])
        @purchase.purchase_date_ms = params[:purchaseTime]
        if @purchase.save
          case @purchase.in_app_product.name
            when "#{univ_text}_item_30"
              coin_to_be_added = 30
            when "#{univ_text}_item_90"
              coin_to_be_added = 90
            when "#{univ_text}_item_130"
              coin_to_be_added = 130
            when "#{univ_text}_item_210"
              coin_to_be_added = 210
            else
              coin_to_be_added = 0
          end

          user_detail.update(coin: original_coin + coin_to_be_added)
          render json: user_detail, status: 200
        else
          render nothing: true, status: 500
        end
      else
        user_detail = current_user.user_detail
        original_coin = user_detail.wallet.coin

        receipt = params[:receipt]
        logger.info "verifying receipt: #{receipt}"
        url = URI.parse('https://buy.itunes.apple.com/')
        req = Net::HTTP::Post.new('/verifyReceipt', initheader = { 'Content-Type' => 'application/json'})
        payload = {
            'receipt-data': receipt
        }
        req.body = payload.to_json
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        res = http.request(req)
        result = JSON.parse(res.body)
        logger.info "verifying receipt result: #{result}"

        if res.code.to_i == 200 and result['status'] == 0
          @purchase = Purchase.new
          @purchase.receipt_b64 = receipt
          @purchase.user = current_user
          @purchase.in_app_product = InAppProduct.find_by_name(result['receipt']['in_app'][0]['product_id'])
          @purchase.attributes = result['receipt'].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          @purchase.attributes = result['receipt']['in_app'][0].reject{|k,v| !@purchase.attributes.keys.member?(k.to_s)}
          @purchase.download_id = 0
          if @purchase.save
            case @purchase.in_app_product.name
              when "#{univ_text}_item_30"
                coin_to_be_added = 30
              when "#{univ_text}_item_90"
                coin_to_be_added = 90
              when "#{univ_text}_item_130"
                coin_to_be_added = 130
              when "#{univ_text}_item_210"
                coin_to_be_added = 210
              else
                coin_to_be_added = 0
            end

            user_detail.update(coin: original_coin + coin_to_be_added)
            render json: user_detail, status: 200
          else
            render nothing: true, status: 500
          end
        elsif res.code.to_i == 200 and result['status'] > 0
          render json: {status: result['status']}, status: 403
        else
          render nothing: true, status: 403
        end
      end
    else
      render nothing: true, status: 401
    end
  end
end
