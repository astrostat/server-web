class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :verify_sms_verification
  before_action :verify_user_detail

  def verify_sms_verification
    redirect_to new_phone_number_path if user_signed_in? && current_user.sms_verification == false
  end

  def verify_user_detail
    redirect_to new_user_detail_path if user_signed_in? && current_user.sms_verification == true && current_user.user_detail == nil
  end

end
