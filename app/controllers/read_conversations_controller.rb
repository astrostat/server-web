class ReadConversationsController < ApplicationController
  def update
    receipt_code = params[:read_conversations][:receipt_code]
    options = { user_id: current_user.id, receipt_code: receipt_code }
    ActionCable.server.broadcast("read_conversations_#{params[:id]}", options)
    receipts = current_user.mailbox.conversations.find(params[:id]).receipts_for(current_user)
    receipts.each do |unread_receipt|
      unless unread_receipt.is_read
        unread_receipt.update(is_read: true) # 메세지 읽음으로 표시
      end
    end
  end
end
