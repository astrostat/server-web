class BadassReportsController < ApplicationController
  def create
    conversation = Mailboxer::Conversation.find(params[:conversation_id])
    badass_user = (conversation.recipients - [current_user]).first
    report = BadassReport.new(reported_user: current_user)
    report.conversation = conversation
    report.badass_user = badass_user
    if report.save
      redirect_to conversations_path, notice: '대화 내용을 신고하였습니다.'
    else
      redirect_to conversations_path, status: 400, notice: '개발자에게 문의해주세요. ※신고기능관련※'
    end
  end
end
