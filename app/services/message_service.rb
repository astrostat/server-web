class MessageService

  def initialize(user)
    @user = user
  end

  def sendable(recipient)

  end

  def find_chat(recipient)
    @user.mailbox.sentbox.each do |sentbox|
      sentbox.recipients.each do |receiver|
        return sentbox if receiver == recipient
      end
    end
  end

  def black_list

  end

  def sendings # 내 입장에서 메세지를 보냈던 사람들, 구매한 상품들 중 돈을내고 메세지를 보낸 상품들을 말함
    receivers = []
    @user.mailbox.sentbox.each do |sentbox|
      sentbox.recipients.each do |receiver|
        receivers << receiver if proper_receiver(receivers, receiver)
      end
    end
    receivers
  end

  def receivings # 내 입장에서 메세지를 받았던 사람들, 구매없이 갑자기 상대방의 구매로 메세지를 받은 상품들을 말함. 이 상품들을 소개에서 지워야한다. loverletters 의 부분집합임.
    senders = []
    @user.mailbox.inbox.each do |sentbox|
      sentbox.recipients.each do |receiver|
        senders << receiver if proper_sender(senders, receiver)
      end
    end
    senders
  end

  private

    def proper_receiver(receivers, receiver)
      return false if receiver == @user
      return false if receivers.include?(receiver)
      receiver
    end

    def proper_sender(senders, sender)
      return false if senders == @user
      return false if senders.include?(sender)
      sender
    end

end