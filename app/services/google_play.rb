class GooglePlay

  def initialize(purchase)
    @packageName = 'com.chalnative'
    @productId = purchase.in_app_product.name if purchase.in_app_product.present?
    @subscriptionId = purchase.in_app_subscription.name if purchase.in_app_subscription.present?
    @token = purchase.purchase_token
  end

  def verify_product
    return true ## 고쳐야됨
    product_url = product_url(@packageName, @productId, @token)
    uri = URI.parse(product_url)
    res = JSON.parse(Net::HTTP.get(uri))
    byebug
    if res['purchaseState'] == 0
      true
    else
      false
    end
  end

  def verify_subscription
    return true ## 고쳐야됨
    product_url = subscription_url(@packageName, @subscriptionId, @token)
    uri = URI.parse(product_url)
    res = JSON.parse(Net::HTTP.get(uri))
    if res['paymentState'] == 1
      true
    else
      false
    end
  end

  def product_url(packageName, productId, token)
    "https://www.googleapis.com/androidpublisher/v2/applications/#{packageName}/purchases/products/#{productId}/tokens/#{token}"
  end

  def subscription_url(packageName, subscriptionId, token)
    "https://www.googleapis.com/androidpublisher/v2/applications/#{packageName}/purchases/subscriptions/#{subscriptionId}/tokens/#{token}"
  end

  def auth
    uri = URI.parse('www.googleapis.com/oauth2/v4/token')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/x-www-form-urlencoded'})
    http.use_ssl = true if url =~ /^https/
    response = http.request(request).body.force_encoding('UTF-8')
    if response.nil?
      flash[:error] = '요청하신 값을 가져오는데 실패하였습니다. 에러 : http_xform_post_job failed'
    else
      response
    end
  end

end