class ProductsService
  def initialize(user)
    @user = user
  end

  def rentables
    products.sample(2) #아직 구매하지 않은 상품중에 2개 추출해서 렌탈셍성
  end

  def change_rentals
    @user.hired_rentals.update_all(already_shown: true) #기존에 렌트한 것들 삭제
    rentables.each do |p|
      Rental.create(hire: @user, hired_user: p)
    end
  end

  def rentals
    if check_in_after(1.years)
      change_rentals
    end
    @user.reload
    @user.hired_rentals.where(already_shown: false)
  end

  def extra_rentals # 돈을 내고 추가 렌탈요청하는 부분
    rentables.each do |p|
      Rental.create(
          {
              hire: @user, hired_user: p
          }
      )
    end
  end

  def products # 직접 구매한 상품 전체와, 직전에 소개 된 렌탈 전체와, 내게 메세지를 보낸 상품들은 제외 시킨 여집합
    purchased = [@user.id] # 자기제거
    @user.proposes.each do |propose| # 이미 구매한 상품 제거
      purchased << propose.matching.receiver.id # if @user.user_detail.gender != propose.matching.receiver.user_detail.gender
    end
    msg = MessageService.new(@user)
    msg.receivings.each do |sender| # 나한테 메세지를 이미 보낸 사람 제거 loveletters 에서 제외시키면 안된다. 나를 소개 받고도 메세지를 딱히 안보낸 사람도 소개는 되야함.
      purchased << sender.id
    end
    @user.hired_rentals.each do |rent| # 모든 렌탈 제거 (리셋되서 안보이는 렌탈과 보고있는 렌탈 모두)
      purchased << rent.hired_user.id
    end
    User.joins(:user_detail).where.not(id: purchased, 'user_details.gender' => @user.user_detail.gender, admin_verification: false, ) # 관리자가 사진/올린정보/음성파일 업로드 후 관리자 승인을 해주지 않으면 소개되지 않도록 한다.
  end

  def purchased
    purchased = [] # 자기제거
    @user.proposes.each do |propose|
      purchased << propose.matching.receiver.id
    end
    User.where(id: purchased)
  end

  private

  def check_in_after(duration)
    check_in = @user.check_in_time
    if check_in.nil? || check_in + duration < Time.zone.now
      @user.check_in_time = Time.zone.now
      @user.save
      return true
    end
    false
  end
end
