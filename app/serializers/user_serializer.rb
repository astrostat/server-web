class UserSerializer < ActiveModel::Serializer
  def attributes(*args)
    object.attributes.symbolize_keys.except(:encrypted_password)
  end

  has_one :wallet
  has_one :user_detail
end
