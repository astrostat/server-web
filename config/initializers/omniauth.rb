Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV["HAWAIIAN_FB_APP_ID"], ENV["HAWAIIAN_FB_APP_SECRET"]
end
