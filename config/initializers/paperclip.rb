Paperclip::Attachment.default_options[:storage] = :s3
Paperclip::Attachment.default_options[:s3_protocol] = :https
Paperclip::Attachment.default_options[:s3_region] = 'ap-northeast-2'
Paperclip::Attachment.default_options[:s3_host_name] = 's3-ap-northeast-2.amazonaws.com'
Paperclip::Attachment.default_options[:s3_credentials] = {
    :bucket => ENV['HAWAIIAN_BUCKET_NAME'],
    :access_key_id => ENV['HAWAIIAN_ACCESS_KEY'],
    :secret_access_key => ENV['HAWAIIAN_SECRET_KEY']
}

Paperclip.interpolates :user_id  do |attachment, style|
  attachment.instance.user.id
end
