RailsAdmin.config do |config|
  config.total_columns_width = 1200
  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'User' do
    object_label_method do
      :user_label_method
    end

    list do
      field :user_detail do
        label '유저정보'
      end
      field :phone_number do
        label '전화번호'
      end
      field :sms_verification do
        label '번호인증'
      end
      field :admin_verification do
        label '관리자인증'
      end
      field :record_request do
        label '인터뷰요청'
      end
      field :wallet do
        label '지갑'
      end
    end
  end

  config.model 'Record' do
    label '인터뷰요청'

    list do
      exclude_fields :updated_at
    end
  end

  config.model 'PhoneNumber' do
    label '전화번호'

    object_label_method do
      :phone_number_label_method
    end
  end

  config.model 'Wallet' do
    object_label_method do
      :wallet_label_method
    end
  end

  config.model 'UserDetail' do
    object_label_method do
      :user_detail_label_method
    end
  end

  config.model Mailboxer::Message do
    object_label_method do
      :mailboxer_message_label_method
    end
  end

  def user_label_method
    "#{self.phone_number.present? ? self.phone_number.phone_number : '번호없음'}(#{self.user_detail.present? ? self.user_detail.nickname : '정보없음'})(#{self.wallet.coin}보유)"
  end

  def user_detail_label_method
    "#{self.nickname}"
  end

  def phone_number_label_method
    "#{self.phone_number}"
  end

  def wallet_label_method
    "#{self.coin}"
  end

  def mailboxer_message_label_method
    "#{self.sender.name} : #{self.body}<hr>".html_safe
  end

end
