Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  namespace :api do
    get 'bank/wallet'
    get 'purchase/call'
    get 'purchase/expense'
    get 'user/information'
    get 'user/profile_image'
    get 'chat/call'
    get 'chat/friend'
    post 'purchase/verify_product'
    post 'purchase/verify_subscription'
  end

  devise_for :staffs
  devise_for :users, controllers: { registrations: 'users/registrations', :omniauth_callbacks => 'users/omniauth_callbacks' }

  resources :conversations, only: [:index, :show, :new, :create] do
    collection do
      get :inbox
      get :sentbox
    end
    member do
      post :reply
      post :trash
      post :untrash
    end
  end

  resources :read_conversations, only: [:update]

  resources :matchings, only: [:index, :new, :create, :show, :destroy]

  resources :friends, only: [:index, :show]
  resource :profile, only: [:show] do
    get 'friend/:id' => 'profiles#friend'
  end
  resource :record

  resources :user_details do
    collection do
      post :image_upload
    end
  end

  resources :phone_numbers, only: [:new, :create]
  resources :verify_phone_numbers, only: [:index, :create]

  resources :paygates
  resources :badass_reports, only: [:create]
  get 'payment/payform'
  post 'payment/gateway'
  get 'purchase/verify' # in app purchase routing

  get 'users/:user_key/mobile' => 'paygates#mobile'

  authenticated :user do
    root 'matchings#index'
  end

  get 'app/index'
  root 'app#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
